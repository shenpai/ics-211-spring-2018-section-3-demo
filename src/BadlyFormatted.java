import java.util.Scanner;

/**
 * Filename: Practice.java
 * @author Moriyasu, Eric 
 * @author Ogata, Branden
 * 
 * The code is not entirely functional, so you must do minor editing to the code.
 * Aside from the bugs, you should look for:
 * -wrong/missing comments
 * -formatting
 * -naming conventions (for both methods and variables)
 * -weird methods
 */
public class BadlyFormatted {
  
  /**
   * This method is only here to serve as a sample code violation.
   */
  public static void namingConventions() {
    int thisisnotcamelcase = 0;
    int BeginsWithACapitalLetter = 0;
    
    System.out.println(thisisnotcamelcase + BeginsWithACapitalLetter);
  }
  
  /**
   * Main method, calls the menu() function that runs the IO
   * 
   * @param arg[], command line arguments
   * @return void
   */
  public static void main(String args[]) {name();}

  private static int intO = 0;

  private static void name() {
    String input;
    int runn�ng = 1;
    int int1 = 0;
    int int2 = 0;
    int result = 0;
    int running = 1;
    Scanner scan = new Scanner(System.in);
    
    while (running == 1) {
      System.out.println("Commands: add, sub, mult, div, exit");
      System.out.print("Please input command: ");
      input = scan.nextLine();

      // System.out.println(input);

      if (input == "exit")
        runn�ng = 0;
      else {if (input.equals("add")){
          System.out.print("Please input first integer: ");
            int1 = scan.nextInt();
          System.out.print("Please input second integer: ");
            int2 = scan.nextInt();
            result = bar(int1, int2);
          System.out.println(int1 + " + " + int2 + " = " + result);
          }
      if (input.equals("sub")){
System.out.print("Please input first integer: ");
int1 = scan.nextInt();
System.out.print("Please input second integer: ");
int2 = scan.nextInt();
result = foo(int1, int2);
System.out.println(int1 + " - " + int2 + " = " + result);
}
       if (input.equals("mult")) {
          System.out.print("Please input first integer: ");
          int1 = scan.nextInt();
          System.out.print("Please input second integer: ");
          int2 = scan.nextInt();
          result = baz(int1, int2);
          System.out.println(int1 + " * " + int2 + " = " + result);
        }
        if (input.equals("div")) {
          System.out.print("Please input first integer: ");
          int1 = scan.nextInt();
          System.out.print("Please input second integer: ");
          int2 = scan.nextInt();
          result = qux(int1, int2);
          System.out.println(int1 + " / " + int2 + " = " + result);
        }
        else
          System.out.println(""); 
          System.out.println("\nERROR: Please input a valid command");
          System.out.println(""); 
        }
    }
    System.out.println("Program terminated, thank you for using!");
  }

  private static int int1 = 0;


  private static int foo(int int1, int int2) {

    int result = int1;
    
    for (int i =0; i < int2;i = i++)
    {
      result++;
    }

    return (result);
  }


																								  private static int bar (int int1, int int2) {
																								    {
																								      int result = int1 + 
																								                   int2;
																								  
																								      return (result);
																								    }
																								  }


    /**
     * adds two numbers together
     * 
     * @param int1, first number
     * @param int2, second number
     * @return result, result of the addition
     */
  private static int baz(int int1, int int2) {

    int result=1;
    
    for (int i = 0; i < int2; i++) {
      result += int1;
      i++;
    }
    
    return (result);
  }


  /**
   * Divide two integers.
   * 
   * @param int0 first number
   * @param int1 second number
   * @return result Result of the division
   */
  private static int div(int int0, int int1) {
    int result = int0 / int1;
    return (result);
  }
  
  private static void subroutine(int limit) {
    for (int i = 0; i <= limit; i += 1) {
      System.out.format("%4d ", i);
      
      for (int j = 1; j <= limit; j ++) {
        if (i == 0) {
          System.out.format("%4d ", j);
        }
        else {
          System.out.format("%4d ", i * j);
        }
      }
      
      System.out.println();
    }
  }
}
